from flask import Blueprint, render_template, request, jsonify
from . import cache
# import concurrent.futures
from yfinance import Ticker
# from IBAPI_CODE.my_code import return_the_order_object, client
from .indic_calcu import *
import datetime


views = Blueprint("views", __name__)


def calculate_the_profit_and_loss(entry_price, exit_price, direction):
    total_profit = cache.get('profit_trade')
    total_loss = cache.get('loss_trade')
    if direction == "BUY":
        profit_or_loss = exit_price - entry_price
    else:
        profit_or_loss = entry_price - exit_price
    if profit_or_loss > 0:
        total_profit += 1
    else:
        total_loss += 1
    cache.set('profit_trade', total_profit, timeout=7_500)
    cache.set('loss_trade', total_loss, timeout=7_500)
    return None


def return_price():
    the_run_way_session = read_json_file()
    if the_run_way_session[0]:
        the_json_needed = the_run_way_session[-1]
        close_open = the_json_needed['C_O']
        live_bar_values = (client.get_bars(
            stock=the_json_needed['stock_names'], barSize=the_json_needed[close_open]))
        Dict_frame = {"Close":   [x.close for x in live_bar_values][-1]}
        return Dict_frame

###################################################################################


@views.route('/')
def home():
    cache.set('trade_counter', 0, timeout=7_200)
    cache.set('profit_trade', 0, timeout=7_500)
    cache.set('loss_trade', 0, timeout=7_500)

    the_net_pnl = return_the_net_pnl()

    cache.set("Net_pnl", the_net_pnl['RealizedPnL'], timeout=7_500) if the_net_pnl != None else cache.set(
        "Net_pnl", 0, timeout=7_500)

    print("TRADE COUNT HAS BEEN SET")
    return render_template('home.html')


@views.route('/close_p_close_l')
def close_profit_close_loss():

    the_number_value = cache.get('Start_trade_session')

    if the_number_value is None:
        return jsonify({
            'close': None, 'signal': None
        })
    else:
        the_price = the_number_value[0]
        the_signal = the_number_value[-1]
        the_current_price = return_price()['Close']

        return jsonify({
            'close': the_current_price, 'signal': the_signal,
            'old': the_price
        })


@views.route('/count_of_trade')
def count_of_trade():
    the_counter = cache.get('trade_counter')
    total_profit = cache.get('profit_trade')
    total_loss = cache.get('loss_trade')

    the_new_pnl = return_the_net_pnl()

    the_old_pnl = cache.get("Net_pnl")

    the_new_pnl = abs(the_old_pnl - the_new_pnl['RealizedPnL'])
    cache.set("Net_pnl", the_old_pnl, timeout=7_500)

    if count_of_trade is None:
        return jsonify({"trade_count": 0, 'netPnl': the_new_pnl,
                        'profit_trade': total_profit, 'loss_trade': total_loss})
    else:
        return jsonify({"trade_count": the_counter, 'netPnl': the_new_pnl,
                        'profit_trade': total_profit, 'loss_trade': total_loss})


@views.route('/stock_basic_details/<name>')
def stock_basic_details(name: str):
    current_time = datetime.datetime.now().time().strftime("%H:%M:%S")
    if name == "Stock":
        return jsonify({"current_price": None, "Dec": current_time})
    stock = Ticker(name)
    current_price = round(stock.history().tail(1)["Close"].iloc[0], 2)
    current_time = datetime.datetime.now().time().strftime("%H:%M:%S")
    return jsonify({"current_price": "%.2f" % (current_price),
                    "Dec": "(%s) (%s) %.2f" % (name, current_time, current_price)})


@views.route('/get_information_', methods=['POST'])
def save_file():

    value = eval(request.json)
    from pprint import pprint
    pprint(value)
    print(value['stock_names'])

    time_cache_data = cache.get('cached_time')
    print(type(value))
    if time_cache_data is None:
        value['C_O'] = "RSI_OPEN"
        cache.set('cached_time', "RSI_OPEN",  timeout=3600)
    else:
        value['C_O'] = time_cache_data

    print("APIII CALLL")

    print(value['C_O'])

    old_json_values = read_json_file()

    if old_json_values[0]:

        the_all_old_values_of_live = old_json_values[-1]

        if the_all_old_values_of_live["indicator_all_key"]['Trade'] == True:
            # if the_all_old_values_of_live["indicator_all_key"]['Trade'] == True or value['C_O'] == "RSI_CLOSE":
            """ A TRADE SIGNAL IS TRUE WAITING FOR THE NEXT Confirmation """

            final_signal = name_of_indicator(value, previous=1)

            value['indicator_all_key'] = final_signal

            if value['C_O'] == "RSI_OPEN" and final_signal['take_trade'] == True:
                cache.set('cached_time', "RSI_CLOSE",  timeout=5600)

                the_signal = value['indicator_all_key']['SuperTrend_SIGNAL']

                save_signal = {"RSI_OPEN": the_signal}

                cache.set('signal_of_rsi', save_signal, timeout=7_200)

                cache.set('Start_trade_session', [return_price()[
                          'Close'], the_signal], timeout=7_200)
                # cache.set('End_trade_session',0 , timeout=7_200)
                take_order(values=value)

                return jsonify({"name": "RSI_CLOSE"})

            if value['C_O'] == "RSI_CLOSE" and (
                    final_signal['SuperTrend_SIGNAL'] !=
                    cache.get('signal_of_rsi')['RSI_OPEN']):

                cache.set('cached_time', "RSI_OPEN",  timeout=5600)

                the_number_value = cache.get('Start_trade_session')

                the_rsi_start_price = the_number_value[0]
                the_rsi_start_signal = the_number_value[-1]
                the_end_price = return_price()['Close']

                calculate_the_profit_and_loss(
                    direction=the_rsi_start_signal,
                    entry_price=the_rsi_start_price,
                    exit_price=the_end_price
                )

                take_order(values=value)

                the_counter = cache.get('trade_counter')
                the_counter += 1
                cache.set('trade_counter', the_counter, timeout=7_200)

                return jsonify({"name": "RSI_OPEN"})

            if final_signal['take_trade'] == False:
                save_file_json(value)
        else:
            value['indicator_all_key'] = name_of_indicator(value)
            save_file_json(value)

    if not old_json_values[0]:
        value['indicator_all_key'] = name_of_indicator(value)
        save_file_json(value)

    return jsonify({"name": True})


@views.route('/the_stop_loss_and_stop_prof_done', methods=['POST'])
def stop_loss_stop_prof():

    the_json_value_of_prev_signal = (request.json)
    print("%"*10)
    the_t = close_order(the_json_value_of_prev_signal)
    if the_t:
        the_number_value = cache.get('Start_trade_session')
        the_rsi_start_price = the_number_value[0]
        the_rsi_start_signal = the_number_value[-1]
        the_end_price = return_price()['Close']

        calculate_the_profit_and_loss(
            direction=the_rsi_start_signal,
            entry_price=the_rsi_start_price,
            exit_price=the_end_price
        )
        cache.set('cached_time', "RSI_OPEN",  timeout=5600)
        return jsonify({
            'the_system': True
        })
    else:
        return jsonify({
            'the_system': False
        })


@views.route('/close')
def close():
    client.disconnect()
    return jsonify({"DISCONNECT": True})
