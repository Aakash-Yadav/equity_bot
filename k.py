import time
import random
from pynput.mouse import Controller

# Create a mouse controller object
mouse = Controller()

# Define the screen dimensions (you may need to adjust these)
screen_width = 1920
screen_height = 1080

while True:
    try:
        # Generate random mouse coordinates within the screen boundaries
        x = random.randint(0, screen_width)
        y = random.randint(0, screen_height)

        # Move the mouse cursor to the random coordinates
        mouse.position = (x, y)

        # Sleep for 30 seconds before the next move
        time.sleep(30)

    except KeyboardInterrupt:
        break
