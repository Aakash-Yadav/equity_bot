
from webapp import app as app1 
from API import api_app as app2 
from IBAPI_CODE.my_code import start_client,client
import threading
from os import path,remove
import pathlib

JSON_FILE = str(pathlib.Path().absolute() / 'JsonLogs' / 'information.json')

del_old_json_file = lambda: remove(JSON_FILE) if path.isfile(JSON_FILE) else 0 

def run_app(app, port):
    app.run(host='0.0.0.0', port=port)

if __name__ == "__main__":
    
    del_old_json_file()
    
    # Define ports for the two apps
    port1 = 5000
    port2 = 5001

    thread1 = threading.Thread(target=run_app, args=(app1, port1))
    thread2 = threading.Thread(target=run_app, args=(app2, port2))
    
    thread3 = threading.Thread(target=start_client,args=(client,));

    # Start the threads
    thread1.start()
    thread2.start()
    thread3.start()

    # print(init_data_feed("ES",['3 mins'],client,True))
    # Wait for threads 
    thread1.join()
    thread2.join()
    thread3.join()
