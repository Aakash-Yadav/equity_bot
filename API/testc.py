from ibapi.client import *
from ibapi.wrapper import *

from time import sleep

class TestApp(EClient, EWrapper):
  def __init__(self):
    EClient.__init__(self, self)

  def nextValidId(self, orderId: OrderId):
    
    mycontract = Contract()
    mycontract.symbol = "AAPL"
    mycontract.secType = "STK"    
    mycontract.exchange = "SMART"
    mycontract.currency = "USD"
    
    "TSLA"
    
    mycontract_1 = Contract()
    mycontract_1.symbol = "TSLA"
    mycontract_1.secType = "STK"    
    mycontract_1.exchange = "SMART"
    mycontract_1.currency = "USD"

    self.reqContractDetails(orderId, mycontract)
    sleep(5)
    self.reqContractDetails(orderId+2, mycontract_1)

  def contractDetails(self, reqId: int, contractDetails: ContractDetails):
    print(contractDetails.contract)
    
    from datetime import datetime,timezone
    expiration_date_time = datetime.strptime("20230825-15:59:00", "%Y%m%d-%H:%M:%S")
        
    order = Order()
    order.action = "BUY"
    order.orderType = "MKT"
    # order.tif = "GTD"
    order.totalQuantity = 1
    order.lmtPrice = 0.5;
    order.orderId = reqId 
    # order.goodTillDate = expiration_date_time.replace(tzinfo=timezone.utc).strftime("%Y%m%d-%H:%M:%S")
    order.eTradeOnly = False
    # order.filledQuantity = False
    order.eTradeOnly = False
    order.firmQuoteOnly = False
    order.transmit = True

    self.placeOrder(order.orderId, contractDetails.contract, order)
    
    # self.placeOrder(order.orderId, contractDetails.contract, order)


  def openOrder(self, orderId: OrderId, contract: Contract, order: Order, orderState: OrderState):
    print(f"openOrder. orderId: {orderId}, contract: {contract}, order: {order}")

  def orderStatus(self, orderId: OrderId, status: str, filled, remaining, avgFillPrice: float, permId: int, parentId: int, lastFillPrice: float, clientId: int, whyHeld: str, mktCapPrice: float):
    print(f"orderId: {orderId}, status: {status}, filled: {filled}, remaining: {remaining}, avgFillPrice: {avgFillPrice}, permId: {permId}, parentId: {parentId}, lastFillPrice: {lastFillPrice}, clientId: {clientId}, whyHeld: {whyHeld}, mktCapPrice: {mktCapPrice}")

  def execDetails(self, reqId: int, contract: Contract, execution: Execution):
    print(f"reqId: {reqId}, contract: {contract}, execution: {execution}")

app = TestApp()
app.connect("127.0.0.1", 7497, 110)
app.run()